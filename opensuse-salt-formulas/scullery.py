#!/usr/bin/python3

from argparse import ArgumentParser
from configparser import ConfigParser
import logging
import os
import sys

try:
    import vagrant
except ImportError as myerror:
    print('Could not load python-vagrant')
    sys.exit(1)

argparser = ArgumentParser()
config = ConfigParser()
env = os.environ.copy()

arggroup = argparser.add_mutually_exclusive_group()
argparser.add_argument('--debug', help='Print extremely verbose output', action='store_const', dest='loglevel', const=logging.DEBUG, default=logging.INFO)
argparser.add_argument('--config', help='Specify the configuration file to use', default='{}/scullery.ini'.format(os.path.abspath(os.path.dirname(__file__))))
argparser.add_argument('--suite', help='Specify the suite to run', required=True)
arggroup.add_argument('--stop', help='Stop running machines', action='store_true')
arggroup.add_argument('--test', help='Start machines and run tests', action='store_true')
arggroup.add_argument('--status', help='Get Vagrant deployment status', action='store_true')

args = argparser.parse_args()
config.read(args.config)

vmprefix = 'scullery'

def _abort(msg):
    log.error(msg)
    sys.exit(1)

def _config():
    configmap = {'box': {}, 'suites': {}}
    if not config.options('box'):
        _abort('No "box" section found in the configuration file')
    for option in config.options('box'):
        configmap['box'][option] = config.get('box', option)
    suites = [section for section in config.sections() if section.startswith('suite.')]
    if not len(suites):
        _abort('No suites configured')
    log.debug('Suites: {}'.format(str(suites)))
    for section in suites:
        suite = section.replace('suite.', '')
        configmap['suites'][suite] = {}
        for option in config.options(section):
            if option in ['masters', 'minions']:
                value = config.getint(section, option)
            else:
                value = config.get(section, option)
            configmap['suites'][suite][option] = value
    log.debug('Config map: {}'.format(str(configmap)))
    return configmap

def _vagrant(quiet=False):
    return vagrant.Vagrant(quiet_stdout=False, quiet_stderr=quiet)

def genvms(flavor, amount):
    vms = []
    for i in range(amount):
        vms.append('{}-{}{}'.format(vmprefix, flavor, i))
    return vms

def vagrant_env(box_name, box_image, minions=None, masters=None, vagrantfile=None):
    fh = open('.scullery_env', 'w')
    if vagrantfile is not None:
        env['VAGRANT_VAGRANTFILE'] = vagrantfile
        fh.write('VAGRANT_VAGRANTFILE={}\n'.format(vagrantfile))
    env['SCULLERY_BOX_NAME'] = box_name
    env['SCULLERY_BOX_IMAGE'] = box_image
    fh.write('SCULLERY_BOX_NAME={}\nSCULLERY_BOX_IMAGE={}\n'.format(box_name, box_image))
    if masters is not None:
        env_masters = ','.join(masters)
        env['SCULLERY_MASTERS'] = env_masters
        fh.write('SCULLERY_MASTERS={}\n'.format(env_masters))
    if minions is not None:
        env_minions = ','.join(minions)
        env['SCULLERY_MINIONS'] = env_minions
        fh.write('SCULLERY_MINIONS={}\n'.format(env_minions))
    #log.debug('Environment is: {}'.format(str(env)))
    fh.close()

    v.env = env

def vagrant_isup(suite):
    ok = 0
    nok = 0
    statuses = v.status()
    total = len(statuses)
    for status in statuses:
        if status.state == 'running':
            ok += 1
        else:
            nok +=1
    if ok == total:
        return True, None
    elif nok == total:
        return False, True
    else:
        return False, False

def main_interactive():
    configmap = _config()
    box = configmap['box']
    suites = configmap['suites']
    suite = args.suite
    if suite not in suites:
        _abort('No suite named {}'.format(suite))
    suiteconf = configmap['suites'][suite]
    minions = None
    masters = None
    if suiteconf.get('minions', 0) > 0:
        minions = genvms('minion', suiteconf['minions'])
    if suiteconf.get('masters', 0) > 0:
        masters = genvms('master', suiteconf['masters'])
    vagrant_env(box['name'], box['image'], minions, masters, box['file'])
    if args.status:
        log.info('Status report: {}'.format(v.status()))
        return True
    status = vagrant_isup(suite)
    if status[0] is True and status[1] is None:
        if args.stop is True:
            log.info('Destroying machines ...')
            v.destroy()
            if vagrant_isup(suite)[0] is False:
                log.debug('OK')
            else:
                _abort('Destruction failed')
        else:
            log.info('Deployment is already running')
    elif status[0] is False:
        if status[1] is True:
            log.debug('Deployment is not running')
        elif status[1] is False:
            log.warning('Deployment is in an inconsistent state, destroying ...')
            try:
                v.destroy()
            except Exception as myerror:
                log.exception(myerror)
                _abort('Unhandled error')

        if args.stop is False:
            log.info('Launching {} ...'.format(suite))
            v.up()
            if vagrant_isup(suite)[0] is True:
                log.debug('OK')
            else:
                _abort('Start failed')

if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(levelname)s - %(funcName)s: %(message)s', datefmt='%H:%M:%S')
    log = logging.getLogger('scullery')
    log.setLevel(args.loglevel)
    log.debug(args)
    if args.loglevel == logging.INFO:
        log_stderr = True
    else:
        log_stderr = False
    v = _vagrant(log_stderr)

    main_interactive()
else:
    v = _vagrant()
